export default class MyThing {
  constructor() {
    console.log(this);
    if (!this.__proto__.singleton) {
      this.init();
      this.__proto__.singleton = this;
    }
    return this.__proto__.singleton;
  }

  init() {
    this.prop1 = 'hello';
  }

  method1() {}
}



